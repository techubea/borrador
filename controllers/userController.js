const io = require('../io');
const crypt = require('../crypt');
const requestJson = require('request-json');

const baseMlabURL = "https://api.mlab.com/api/1/databases/apitechubpj9ed/collections/";
const mLabAPIKey = "apiKey=" + process.env.MLAB_API_KEY;

function getUsersV1(req,res) {
  console.log("GET /apitechu/v1/users");

    var users = require ('../usuarios.json');
    console.log(users.length);

    var top = req.query.$top;
    if (top){
      users = users.slice(0,top);
      console.log("top = " + top);
    }

    var resul;
    var count = req.query.$count;

      (count && count=='true')?resul = {'top': top, 'count': users.length, 'users': users}:resul = {'top': top, 'count': users.lenght, 'users': users};

      res.send(resul);
}


function getUsersV2(req,res) {
  console.log("POST /apitechu/v2/users");

  var httpClient = requestJson.createClient(baseMlabURL);
  console.log("Client created");

  httpClient.get("user?" + mLabAPIKey,
    function(err, resMLab, body) {
      var response = !err ? body : {
        "msg" : "Error obteniendo usuarios"
      }

      res.send(response);
    }
  );
}


function getUsersV2byID(req,res) {
  console.log("GET /apitechu/v2/users/:id");

  var httpClient = requestJson.createClient(baseMlabURL);
  console.log("Client created");

  var id = req.params.id;
  console.log("Id del usuario a traer " + id);
  var query ='q="id":' + id + '}';

  httpClient.get("user?" + query + "&" + mLabAPIKey,
    function(err, resMLab, body) {
      var response = !err ? body : {
        "msg" : "Error obteniendo usuario"
      }

      res.send(response);
    }
  );
}


function createUserV1(req,res) {
  console.log("POST /apitechu/v1/users");

  console.log(req.body.first_name);
  console.log(req.body.last_name);
  console.log(req.body.email);

//    var id = users.length + 1;

  var newUser = {
//      "id": id,
    "first_name": req.body.first_name,
    "last_name": req.body.last_name,
    "email": req.body.email
  }

  var users = require('../usuarios.json');
  users.push(newUser);
  io.writeUserDataToFile(users);
  console.log("Usuario añadido");
}


function createUserV2(req,res) {
  console.log("POST /apitechu/v2/users");

  console.log("id es " + req.body.id);
  console.log("first_name es " + req.body.first_name);
  console.log("last_name es " + req.body.last_name);
  console.log("email es " + req.body.email);
  console.log("password es " + req.body.password);

  var newUser = {
    "id": req.body.id,
    "first_name": req.body.first_name,
    "last_name": req.body.last_name,
    "email": req.body.email,
    "password" : crypt.hash(req.body.password)
  };

  var httpClient = requestJson.createClient(baseMlabURL);
  httpClient.post("user?" + mLabAPIKey, newUser,
    function(err, resMLab, body) {
      console.log("Usuario guardado con éxito");
      res.send({"msg" : "Usuario guardado con éxito"});
    }
  );
}


function deleteUserV1(req, res) {
  console.log("DELETE /apitechu/v1/users/:id");
  console.log("id es " + req.params.id);

  var users = require('../usuarios.json');
  var deleted = false;

  // console.log("Usando for normal");
  // for (var i = 0; i < users.length; i++) {
  //   console.log("comparando " + users[i].id + " y " +  req.params.id);
  //   if (users[i].id == req.params.id) {
  //     console.log("La posicion " + i + " coincide");
  //     users.splice(i, 1);
  //     deleted = true;
  //     break;
  //   }
  // }

  // console.log("Usando for in");
  // for (arrayId in users) {
  //   console.log("comparando " + users[arrayId].id + " y "  + req.params.id);
  //   if (users[arrayId].id == req.params.id) {
  //     console.log("La posicion " + arrayId " coincide");
  //     users.splice(arrayId, 1);
  //     deleted = true;
  //     break;
  //   }
  // }

  // console.log("Usando for of");
  // for (user of users) {
  //   console.log("comparando " + user.id + " y " +  req.params.id);
  //   if (user.id == req.params.id) {
  //     console.log("La posición ? coincide");
  //     // Which one to delete? order is not guaranteed...
  //     deleted = false;
  //     break;
  //   }
  // }

  // console.log("Usando for of 2");
  // // Destructuring, nodeJS v6+
  // for (var [index, user] of users.entries()) {
  //   console.log("comparando " + user.id + " y " +  req.params.id);
  //   if (user.id == req.params.id) {
  //     console.log("La posicion " + index + " coincide");
  //     users.splice(index, 1);
  //     deleted = true;
  //     break;
  //   }
  // }

  // console.log("Usando for of 3");
  // var index = 0;
  // for (user of users) {
  //   console.log("comparando " + user.id + " y " +  req.params.id);
  //   if (user.id == req.params.id) {
  //     console.log("La posición " + index + " coincide");
  //     users.splice(index, 1);
  //     deleted = true;
  //     break;
  //   }
  //   index++;
  // }

  // console.log("Usando Array ForEach");
  // users.forEach(function (user, index) {
  //   console.log("comparando " + user.id + " y " +  req.params.id);
  //   if (user.id == req.params.id) {
  //     console.log("La posicion " + index + " coincide");
  //     users.splice(index, 1);
  //     deleted = true;
  //   }
  // });

  // console.log("Usando Array findIndex");
  // var indexOfElement = users.findIndex(
  //   function(element){
  //     console.log("comparando " + element.id + " y " +   req.params.id);
  //     return element.id == req.params.id
  //   }
  // )
  //
  // console.log("indexOfElement es " + indexOfElement);
  // if (indexOfElement >= 0) {
  //   users.splice(indexOfElement, 1);
  //   deleted = true;
  // }

  if (deleted) {
    // io.writeUserDataToFile(users);
   io.writeUserDataToFile(users);
  }

  var msg = deleted ?
  "Usuario borrado" : "Usuario no encontrado."

  console.log(msg);
  res.send({"msg" : msg});
}


/*
function writeUserDataToFile(data){
    console.log("writeUserDataToFile");

    const fs = require('fs');
    var jsonUserData = JSON.stringify(data);

    fs.writeFile("./usuarios.json", jsonUserData, "utf8",
      function (err){
        if (err){
          console.log(err);
        } else {
          console.log("Usuario persistido");
        }
      }
    )
  }
*/

/*
app.delete('/apitechu/v1/users/:id',
  function (req,res) {
    console.log("DELETE /apitechu/v1/users/:id");
    console.log("La id a modificar es " + req.params.id);

    var users = require ('./usuarios.json');
    users.splice(req.params.id - 1, 1);
    console.log("Usuario borrado");
    io.writeUserDataToFile(users);
  }
)
*/

module.exports.getUsersV1 = getUsersV1;
module.exports.getUsersV2 = getUsersV2;
module.exports.getUsersV2byID = getUsersV2byID;
module.exports.createUserV1 = createUserV1;
module.exports.createUserV2 = createUserV2;
module.exports.deleteUserV1 = deleteUserV1;
