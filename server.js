require ('dotenv').config();

// console.log("Holiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiisssss");
const express = require('express');
const app = express();
const io = require('./io');
// console.log("Añadido IO");

const userController = require('./controllers/userController');
// console.log("Añadido controllers/userController");

const authController = require('./controllers/authController');
// console.log("Añadido controllers/authController");

app.use(express.json());

const port = process.env.PORT || 3000;
app.listen(port);
console.log("API escuchando en el puerto test " + port);

app.get('/apitechu/v1/users', userController.getUsersV1);
app.get('/apitechu/v2/users', userController.getUsersV2);
app.get('/apitechu/v2/users/:id', userController.getUsersV2byID);
app.post('/apitechu/v1/users', userController.createUserV1);
app.post('/apitechu/v2/users', userController.createUserV2);
app.delete('/apitechu/v1/users/:id', userController.deleteUserV1);
app.post('/apitechu/v1/login', authController.loginUsersV1);
// app.post('/apitechu/v1/login', authController.loginV1);




app.get('/apitechu/v1/hello',
  function (req,res) {
    console.log("GET /apitechu/v1/hello");

    res.send({"msg" : "Hola desde API TechU"})
  }
)


app.post('/apitechu/v1/monstruo/:p1/:p2',
  function (req,res) {
      console.log("Parámetros");
      console.log(req.params);

      console.log("Query String");
      console.log(req.query);

      console.log("Headers");
      console.log(req.headers);

      console.log("Body");
      console.log(req.body);
  }
)
